package com.greatlearning.week6.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import com.greatlearning.week6.bean.Favourite;
import com.greatlearning.week6.resource.DbResource;

public class FavouriteDao {


	public List<Favourite> findAllMovies() {
		List<Favourite> listOfFavourite = new ArrayList<>();
		try {
			Class.forName("com.mysql.cj.jdbc.Driver");
			//Connection con = DriverManager.getConnection("jdbc:mysql://localhost:3306/hcl","root","5A4m267p");
			Connection con = DbResource.getDbConnection();
			PreparedStatement pstmt = con.prepareStatement("select * from Favourite");
			ResultSet rs = pstmt.executeQuery();
			while(rs.next()) {
				Favourite m = new Favourite();
					m.setId(rs.getInt(1));
					m.setTitle(rs.getString(2));
					m.setYear(rs.getInt(3));
					m.setGenre(rs.getString(4));
					listOfFavourite.add(m);
			}
			} catch (Exception e) {
				System.out.println("In delete method "+e);
			}
		return listOfFavourite;
}
}
