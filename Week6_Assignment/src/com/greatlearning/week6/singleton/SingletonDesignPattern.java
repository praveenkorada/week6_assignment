package com.greatlearning.week6.singleton;

public class SingletonDesignPattern {
	
	private static SingletonDesignPattern instance;
	
	// private constructor to force use of
    // getInstance() to create Singleton object
	public static SingletonDesignPattern getInstance() {
		
		if(instance == null) {
			instance = new SingletonDesignPattern();
		}
		return instance;
	}

}
