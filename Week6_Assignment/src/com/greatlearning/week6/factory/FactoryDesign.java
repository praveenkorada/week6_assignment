package com.greatlearning.week6.factory;

// FactoryDesignPattern {

interface Movies {
	public void create();
}
class MoviesComing implements Movies{
	public void create() {
		System.out.println("moviescoming");
	}
}
class MoviesInTheaters implements Movies{
	public void create() {
		System.out.println("MoviesInTheaters");
	}
}
class TopRatedIndia implements Movies{
	public void create() {
		System.out.println("TopRatedIndia");
	}
}
class TopRatedMovies implements Movies{
	public void create() {
		System.out.println("TopRatedMovies");
	}
}
class Favourite implements Movies{
	public void create() {
		System.out.println("Favourite");
	}
}
class MovieFactory {

	public static Movies getInstance(String type) {
		if(type.equalsIgnoreCase("moviescoming")) {
		return new MoviesComing();
		}else if(type.equalsIgnoreCase("moviesintheaters")) {
			return new MoviesInTheaters();
		}else if(type.equalsIgnoreCase("topratedindia")) {
			return new TopRatedIndia();
	    }else if(type.equalsIgnoreCase("topratedmovies")) {
	    	return new TopRatedMovies();
	    }else if(type.equalsIgnoreCase("favourite")) {
	    	return new Favourite();
	
		}else {
			return null;
		}
	} 
}
public class FactoryDesign {
	public static void main(String[] args) {
		Movies mm1 = MovieFactory.getInstance("moviescoming");
		mm1.create();
		Movies mm2 = MovieFactory.getInstance("moviesintheaters");
		mm2.create();
		Movies mm3 = MovieFactory.getInstance("topratedindia");
		mm3.create();
		Movies mm4 = MovieFactory.getInstance("topratedmovies");
		mm4.create();
		Movies mm5 = MovieFactory.getInstance("favourite");
		mm5.create();
		
	}
}