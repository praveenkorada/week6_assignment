create database week6_praveen;
use week6_praveen;

create table MoviesComing(ID int, Title varchar(10),year int,genre varchar(10));

create table MoviesIntheaters(ID int, Title varchar(10),year int,genre varchar(10));

create table TopRatedIndia(ID int, Title varchar(10),year int,genre varchar(10));

create table TopRatedMovies(ID int, Title varchar(10),year int,genre varchar(10));

create  table Favourite(ID int, Title varchar(10),year int,genre varchar(10));

show tables;

insert into moviescoming values(1,'hannah',2017,'drama');
insert into moviescoming values(1,'survivors',2018,'action');
insert into moviescoming values(1,'red',2018,'action');


insert into moviesintheaters values(1,'black',2018,'action');
insert into moviesintheaters values(1,'pain',2018,'thriller');
insert into moviesintheaters values(1,'aiyaary',2020,'crime');

insert into topratedindia values(1,'anand',2005,'drama');
insert into topratedindia values(2,'dangal',2016,'inspire');
insert into topratedindia values(3,'drishym',2013,'thriller');


insert into topratedmovies values(1,'bazigar',1993,'action');
insert into topratedmovies values(2,'24',2016,'drama');
insert into topratedmovies values(3,'akbar',2008,'history');

select *from moviescoming;
select *from moviesintheaters;
select *from topratedindia;
select *from topratedmovies;
